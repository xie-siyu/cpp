#pragma once
#include <iostream>

using namespace std;

class Date
{
public:
	Date(int year = 2000, int month = 1, int day = 1);
	
	void Print()
	{
		cout << _year << "_" << _month << "_" << _day << endl;
	}

	bool operator==(const Date& x);
	bool operator<(const Date& x);
	bool operator<=(const Date& x);
	bool operator>(const Date& x);
	bool operator>=(const Date& x);
	bool operator!=(const Date& x);

	int GetMouthDay(int year, int month);
	Date& operator+=(int day);
	Date operator+(int day);
	Date& operator-=(int day);
	Date operator-(int day);
	
	Date& operator++();
	Date operator++(int);
	Date& operator--();
	Date operator--(int);

	int operator-(const Date& d);



private:
	int _year;
	int _month;
	int _day;
	
};
