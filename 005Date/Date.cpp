#include "Date.h"

Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}

bool Date::operator==(const Date& x)
{
	if (_year == x._year &&
		_month == x._month &&
		_day == x._day)
	{
		return true;
	}
	else
		return false;
}

bool Date::operator<(const Date& x)
{
	if (_year < x._year)
	{
		return true;
	}
	else if (_year == x._year && _month < x._month) 
	{
		return true;
	}
	else if (_year == x._year && _month == x._month && _day < x._day)
	{
		return true;
	}
	
	return false;
	
}

bool Date::operator<=(const Date& x)
{
	return (*this < x || *this == x);
}

bool Date::operator>(const Date& x)
{
	return !(*this <= x);
}

bool Date::operator>=(const Date& x)
{
	return !(*this < x);
}

bool Date::operator!=(const Date& x)
{
	return !(*this == x);
}

int Date::GetMouthDay(int year, int month)
{
	//两点优化
	static int daysArr[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	if (month == 2 && ((year % 4 == 0 && year % 100) || (year % 400 == 0)))
	{
		return 29;
	}
	else
	{
		return daysArr[month];
	}
}

Date& Date::operator+=(int day)
{
	//*this = *this + day;
	//return *this;

	if (day < 0)
	{
		return (*this -= -day);
	}

	_day += day;
	while (_day > GetMouthDay(_year, _month))
	{
		_day -= GetMouthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}
	return *this;
}

Date Date::operator+(int day)
{
	Date tmp(*this);
	tmp += day;

	/*tmp._day += day;
	while (tmp._day > GetMouthDay(tmp._year, tmp._month))
	{
		tmp._day -= GetMouthDay(tmp._year, tmp._month);
		tmp._month++;
		if (tmp._month == 13)
		{
			tmp._month = 1;
			tmp._year++;
		}
	}*/

	return tmp;
}

//前置++
Date& Date::operator++()
{
	*this += 1;
	return *this;
}

//后置++
//加这个int不是为了接收具体的值，而是为了占位，和前置++构成重载
Date Date::operator++(int)
{
	Date tmp = *this;
	*this += 1;

	return tmp;
}

Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return (*this += -day);
	}
	_day -= day;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
		_day += GetMouthDay(_year, _month);
	}
	return *this;
}

Date Date::operator-(int day)
{
	Date tmp(*this);
	tmp -= day;
	return tmp;
}

//前置--
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

//后置--
//自定义类型能用前置拷贝就用前置拷贝
Date Date::operator--(int)
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

//两日期相减
int Date::operator-(const Date& d)
{
	Date max = *this;
	Date min = d;
	int flag = 1;
	if (max < min)
	{
		max = d;
		min = *this;
		flag = -1;
	}
	int n = 0;
	while (max != min)
	{
		min++;
		n++;
	}
	return n * flag;
}