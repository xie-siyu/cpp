#include "Date.h"

void Test1()
{
	Date d1(2024, 11, 20);
	Date d2(2024, 10, 20);
	Date d3(2024, 10, 20);

	//cout << (d1 <= d2) << endl;	//转换成operator<(d1, d2);
	//cout << (d2 <= d1) << endl;
	//cout << (d2 == d3) << endl;
	//cout << (d2 != d3) << endl;

	d1 += 100;
	d2 + 100;
	Date d4(d2 + 100);

	d1.Print();
	d2.Print();
	d4.Print();
}

void Test2()
{
	Date d1(2023, 4, 26);
	// 都要++，前置++返回++以后的对象，后置++返回++之前的对象
	++d1; // d1.operator++()
	d1++; // d1.operator++(0)
}

void Test3()
{
	Date d1(2024, 11, 22);
	d1 -= 22;
	d1.Print();
	Date d2 = d1 - 100;
	d1.Print();
	d2.Print();
	d2 -= -100;
	d2.Print();
	d2 += -100;
	d2.Print();
}

void Test4()
{
	Date d1(2024, 11, 22);
	Date d2(2024, 11, 22);
	Date ret1 = d1--;	//d1.operator--(&d1, 0);
	ret1.Print();
	d1.Print();

	Date ret2 = --d2;	//d2.oprator--(&d2);
	ret2.Print();
	d2.Print();
}

void Test5()
{
	Date d1(2024, 11, 22);
	Date d2(1949, 10, 1);
	Date d3(2000, 8, 25);

	cout << d1 - d2 << endl;
	cout << d3 - d1 << endl;

}

int main()
{
	
	Test5();

	return 0;
}
