#include <iostream>

using namespace std;

//半缺省 —— 从右往左缺省
//void Func(int a, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//}
//
//int main()
//{
//	Func(1);
//	Func(1,2);
//	Func(1,2,3);
//
//	return 0;
//}

//struct Stack
//{
//	int* a; 
//	int top;
//	int capacity;
//};
//
////缺省参数（默认参数）可以做到想传就传，不想传就不传
//void StackInit(struct Stack* pst, int defaultCapacity = 4)
//{
//	pst->a = (int*)malloc(sizeof(int) * defaultCapacity);
//	if (pst->a == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//
//	pst->top = 0;
//	pst->capacity = defaultCapacity;
//}


//void f()
//{
//	cout << "f()" << endl;
//}
//
//void f(int a = 0)
//{
//	cout << "f(int a)" << endl;
//}
//
//int main()
//{
//	f();
//	return 0;
//}