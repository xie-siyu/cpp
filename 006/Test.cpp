#include <iostream>

using namespace std;

#if 0
class Date
{
public:
	Date* operator&()
	{
		//return this;
		return nullptr;
	}
	const Date* operator&()const
	{
		return this;
	}

private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};

int main()
{
	Date d1;
	const Date d2;
	cout << &d1 << endl;
	cout << &d2 << endl;

	return 0;
}
#endif

class A
{
public:
	A(int a = 0)
		: _a(a)
	{
		cout << "A(int a = 0)" << endl;
	}

private:
	int	_a;
};

class B
{
public:
	B(int a, int ref)
		:_ref(ref)
		,_n(1)
		,_x(2)
		,_aobj(10)
	{
		;
	}


private:
	A _aobj;		//没有默认构造
	int& _ref;		//引用
	const int _n;	//const

	int _x = 1;
};

class MyQueue
{
public:

private:
	Stack _pushst;
	Stack _popst;
};

int main()
{
	B bb1(10, 1);	
	B bb2(11, 2);

	return 0;
}