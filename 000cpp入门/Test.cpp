//#include <stdio.h>
//#include <stdlib.h>


//namespace xsy
//{
//	int rand = 1;
//}
//
//int main()
//{
//
//	// ::域作用限定符
//	printf("%p\n", rand);
//	printf("%d\n", bit::rand);
//
//	return 0;
//}

#include <iostream>

//全部展开有风险，日常练习可以
//项目中建议指定访问
//using namespace std;

//展开某个：把常用的展开
using std::cout;
using std::endl;

int main()
{
	cout << "hello world\n" << endl;
	std::cout << "nihao\n" << std::endl;
	return 0;
}
