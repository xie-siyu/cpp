#include <assert.h>
#include <iostream>

using std:: cout;
using std:: endl;


//1、引用做参数：输出型参数，减少拷贝，提高效率

#if 0
typedef struct ListNode
{
	int data;
	struct ListNode* next;
}ListNode, *PList;

void SLPushBack(PList& phead, int x)
{
	//这里是指针PList的引用（别名），不用二级指针了
}
#endif

//2、引用做返回值：除了减少拷贝，还能让返回值有读写功能
#if 0
struct SeqList
{
	int a[100];
	size_t size;
};

//指针更改静态链表
void SLModify(SeqList* ps, int pos, int x)
{
	assert(pos < 100 && pos >= 0);
	ps->a[pos] = x;
}

int SLGet(SeqList* ps, int pos)
{
	assert(pos < 100 && pos >= 0);
	return ps->a[pos];
}

int& SLAt(SeqList* ps, int pos)
{
	assert(pos < 100 && pos >= 0);
	return ps->a[pos];
}

void Test1()
{
	SeqList s;
	SLModify(&s, 0, 1);
	//对第0个位置的值+5
	int ret = SLGet(&s, 0);
	SLModify(&s, 0, ret + 5);
}

void Test2()
{
	SeqList s;
	SLAt(&s, 0) = 1;	//改为0位置处1
	SLAt(&s, 0) += 5;	//0位置处+5
}

int main()
{
	Test1();
	Test2();

	return 0;
}
#endif 


#if 0
//cpp把struct升级成了类,类里面可以定义函数
struct SeqList
{
	int a[100];
	size_t size;

	int& at(int pos)
	{
		assert(pos >= 0 && pos < 100);
		return a[pos];
	}

	int& operator[](int pos)
	{
		assert(pos >= 0 && pos < 100);
		return a[pos];
	}

};

int main()
{
	SeqList s;
	s.at(0) = 1;
	cout << s.at(0) << endl;

	s.at(0) += 5;
	cout << s.at(0) << endl;

	s[1] = 10;
	cout << s[1] << endl;

	s[1]++;
	cout << s[1] << endl;

	return 0;
}
#endif

#if 0
int main()
{
	//引用过程中，权限不能放大
	const int a = 0;
	//int& b = a;

	const int c = 0;
	int d = c;

	//权限可以平移或者缩小
	int x = 0;
	int& y = x;
	const int z = x;
	++x;
	//++z;

	const int& m = 10;

	//类型转换过程中有临时变量，临时变量有常性
	double dd = 1.11;
	int ii = dd;
	//int& rii = dd;
	const int& cii = dd;

	return 0;
}
#endif 


#if 0
int func1()
{
	static int x = 0;
	return x;
}

int& func2()
{
	static int x = 0;
	return x;
}

int main()
{
	int ret = func1();
	//传值返回有临时变量，不能改
	//int& ret1 = func1();		//权限放大
	const int& ret1 = func1();	//权限平移

	int& ret2 = func2();		//权限平移
	const int& rret2 = func2();	//权限缩小

	return 0;
}
#endif

#if 0
int main()
{
	int a = 0;
	int b = a;

	//自动推导c、d的类型
	auto c = a;
	auto d = 1 + 1.11;
	cout << typeid(c).name() << endl;
	cout << typeid(d).name() << endl;

	return 0;
}
#endif

int& Add(int& a, int b)
{
	a += 1;
	b += 2;
	int c = a + b;
	return c;
}
int main()
{
	int x = 3;
	int y = 4;
	cout << Add(x, y) << endl;
	cout << x << endl;
	cout << y << endl;
	cout << ++Add(x, y) << endl;
	cout << x << endl;
	cout << y << endl;
	return 0;
}