#include <iostream>
using namespace std;


#if 0
//分号，return，形参
#define Add(x, y) ((x) + (y))
//宏函数
//优点，不需要建立栈帧，提高效率
//缺点，复制，容易出错，可读性差，不能调试
int main()
{
	for (int i = 0; i < 10000; i++)
	{
		cout << Add(i, i + 1) << endl;
	}
	if (Add(10, 20)) {}

	int a = 1, b = 2;
	Add(a | b, a & b);
	return 0;
}
#endif

#if 0
//内联函数，宏，短小频繁调用的函数
//编译器自己决定是否是内联
//默认debug下，inline不展开内联
inline int Add(int x, int y)
{
	return (x + y) * 10;
}

int main()
{
	for (int i = 0; i < 10000; i++)
	{
		cout << Add(i, i + 1) << endl;
	}
	return 0;
}
//内联函数声明和定义不能分开
#endif

//auto
int main()
{
	int x = 1;
	auto a = &x;
	auto* b = a;
	auto& c = x;

	int arr[] = { 1,2,3 };
	for (auto& i : arr)
	{
		x++;
	}
	for (auto& x : arr)
	{
		cout << x << endl;
	}

	return 0;
}